# Typst.kak

This repository provides [Kakoune](https://kakoune.org) syntax highlighting 
for [Typst](https://typst.app).

## Screenshot:

![A screenshot of a Typst file in Kakoune](./imgs/screen0.png)
