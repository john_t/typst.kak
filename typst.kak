# Setting up all the highlighters for this syntax
# could be expensive, so we'll define them inside a module
# that won't be loaded until we need it.
#
# Because this module might contain a bunch of regexes with
# unbalanced grouping symbols, we'll use some other character
# as a delimiter.
provide-module typst %§
    # Define our highlighters in the shared namespace,
    # so we can link them later.
    add-highlighter shared/typst regions

    # Everything outside a region is a group of highlighters.
    add-highlighter shared/typst/text default-region group
    add-highlighter shared/typst/text/regions regions
    add-highlighter shared/typst/text/strong regex '(?<![\\])\*[^\s].*?(?<=[^\s])(?!\w)\*' 0:+b
    add-highlighter shared/typst/text/emph regex '(?<![\w])_[^\s].*?(?<=[^\s])_(?!\w)' 0:+i
    add-highlighter shared/typst/text/raw regex '(?<![\w])`[^\s].*?(?<=[^\s])`(?!\w)' 0:mono
    add-highlighter shared/typst/text/heading  regex '^\h*=+[^\n]+' 0:title
    add-highlighter shared/typst/text/label regex '<\w+>' 0:meta
    add-highlighter shared/typst/text/ref regex '@\w+' 0:meta
    add-highlighter shared/typst/text/list regex '^\h*-' 0:bullet
    add-highlighter shared/typst/text/enum regex '^\h*\+' 0:bullet
    add-highlighter shared/typst/text/dollars regex '\$' 0:attribute
    add-highlighter shared/typst/text/hash regex '#\w+' 0:variable

    add-highlighter shared/typst/text/regions/term region '^\h*/' '\n' group
    add-highlighter shared/typst/text/regions/term/slash regex '/' 0:operator
    add-highlighter shared/typst/text/regions/term/term regex '(?<=/).+?:' 0:variable

    add-highlighter shared/typst/maths region '\$' '\$' regions
    add-highlighter shared/typst/maths/d default-region group
    add-highlighter shared/typst/maths/d/op regex '[\^\-\\&>/]' 0:operator
    add-highlighter shared/typst/maths/d/dollar regex '\$' 0:operator
    add-highlighter shared/typst/maths/d/variable regex '(\b#\w\b|\b[A-Za-z]{2,}\b)' 0:variable
    add-highlighter shared/typst/maths/string region '"' '(?<!\\)"' fill string

    add-highlighter shared/typst/code region -recurse '\(|\{' '#[^\n]+(\(|\{)' '\)|\}' regions
    add-highlighter shared/typst/code/d default-region group
    add-highlighter shared/typst/code/d/reset fill default
    add-highlighter shared/typst/code/d/number regex \d+(pt|mm|cm|in|em|deg|rad|%|fr)? 0:value
    add-highlighter shared/typst/code/d/auto regex auto 0:value
    add-highlighter shared/typst/code/d/function regex '#?\w+(?=\()' 0:function
    add-highlighter shared/typst/code/d/property regex '#?\w+(?=:)' 0:variable
    add-highlighter shared/typst/code/d/keywords regex "#?\b(let|set|show|if|else|for|in|while|break|continue|return|import|include)\b" 0:keyword
    add-highlighter shared/typst/code/d/let_name regex "(?<=let)\h+\w+" 0:variable
    add-highlighter shared/typst/code/d/dollars regex '\$' 0:attribute
    add-highlighter shared/typst/code/text region -recurse '\[' '\[' '\]' ref typst/text
    add-highlighter shared/typst/code/maths region '\$' '\$' ref typst/maths
    add-highlighter shared/typst/code/string region '"' '(?<!\\)"' fill string

    add-highlighter shared/typst/code2 region '\b#' '$' ref typst/code


    add-highlighter shared/typst/text/regions/line_comment region '//' '\n' fill comment
    add-highlighter shared/typst/maths/line_comment region '//' '\n' fill comment
    add-highlighter shared/typst/code/line_comment region '//' '\n' fill comment

    add-highlighter shared/typst/text/regions/block_comment region '/\*' '\*/' fill comment
    add-highlighter shared/typst/maths/block_comment region '/\*' '\*/' fill comment
    add-highlighter shared/typst/code/block_comment region '/\*' '\*/' fill comment
§

# When a window's `filetype` option is set to this filetype...
hook global WinSetOption filetype=typst %{
    # Ensure our module is loaded, so our highlighters are available
    require-module typst

    # Link our higlighters from the shared namespace
    # into the window scope.
    add-highlighter window/typst ref typst

    hook window InsertChar \n -group typst-indent typst-indent-on-new-line

    # Add a hook that will unlink our highlighters
    # if the `filetype` option changes again.
    hook -once -always window WinSetOption filetype=.* %{
        remove-highlighter window/typst
    }
}

# Lastly, when a buffer is created for a new or existing file,
# and the filename ends with `.typst`...
hook global BufCreate .+\.(typ|typst) %{
    # ...we recognise that as our filetype,
    # so set the `filetype` option!
    set-option buffer filetype typst
}

define-command -hidden typst-indent-on-new-line %§
    evaluate-commands -draft -itersel %¶
        # preserve previous line indent
        try %{ execute-keys -draft <semicolon> K <a-&> }
        # remove trailing white spaces
        try %{ execute-keys -draft k <a-x> s \h+$ <ret> d }
        # brackets
        try %@ execute-keys -draft k x s [\(\[\{]\h*$ <ret> j <a-gt> @
    ¶
§
